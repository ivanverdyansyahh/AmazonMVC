<?php

class User extends Controller
{
    public function index()
    {
        $data['title'] = 'Amazon | User Page';
        $data['company'] = 'Amazon';
        $data['users'] = $this->model('User_model')->tampil();

        $this->view('templates/header', $data);
        $this->view('templates/sidebar');
        $this->view('user/index', $data);
        $this->view('templates/footer');
    }

    public function formUser()
    {
        $data['title'] = 'Amazon | Tambah User Page';
        $data['company'] = 'Amazon';

        $this->view('templates/header', $data);
        $this->view('templates/sidebar');
        $this->view('user/tambah', $data);
        $this->view('templates/footer');
    }

    public function tambah()
    {
        if ($this->model('User_model')->tambah($_POST) > 0) {
            Flasher::setFlash('berhasil', 'ditambahkan', 'success');
            header('Location: ' . BASEURL . 'User');
            exit;
        } else {
            echo "gagal tambah user";
        }
    }
}
