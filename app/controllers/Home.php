<?php

class Home extends Controller
{
    public function index()
    {
        $data['title'] = 'Amazon | Dashboard Page';
        $data['company'] = 'Amazon';

        $this->view('templates/header', $data);
        $this->view('templates/sidebar');
        $this->view('home/index', $data);
        $this->view('templates/footer',);
    }
}
