<?php

class User_model
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function tampil()
    {
        $this->db->query("SELECT * FROM user ORDER BY absen DESC");
        return $this->db->resultSet();
    }

    public function tambah($data)
    {
        $query = "INSERT INTO user VALUES (:absen, :nama, :email, :firstname, :lastname)";

        $this->db->query($query);
        $this->db->bind('absen', $data['absen']);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('email', $data['email']);
        $this->db->bind('firstname', $data['firstname']);
        $this->db->bind('lastname', $data['lastname']);

        $this->db->execute();

        return $this->db->rowCount();
    }
}
