<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 mb-5">
    <!-- <div class="alert alert-success" role="alert">
        A simple success alert—check it out!
    </div> -->

    <?= var_dump($data["users"]); ?>

    <div class="flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
        <h1 class="h2">Data Siswa</h1>
        <div class="wrapper d-flex justify-content-between">
            <p class="col-6 m-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis ducimus voluptatibus reiciendis ex laborum magnam optio in doloribus ratione esse.</p>
            <a href="<?= BASEURL ?>User/formUser" class="btn btn-dark">Tambah Data</a>
        </div>
    </div>


    <div class="table-responsive mt-4">
        <table class="table table-bordered table-sm">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Email</th>
                    <th scope="col">Firstname</th>
                    <th scope="col">Lastname</th>
                </tr>
            </thead>
            <tbody>
                <?= $users = $data["users"]; ?>
                <tr>
                    <td>1,001</td>
                    <td>random</td>
                    <td>data</td>
                    <td>placeholder</td>
                    <td>text</td>
                </tr>
            </tbody>
        </table>
    </div>
</main>