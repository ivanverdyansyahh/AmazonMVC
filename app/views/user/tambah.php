<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 mb-5">
    <div class="flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
        <h1 class="h2">Tambah Data User</h1>
        <p class="col-6 m-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis ducimus voluptatibus reiciendis ex laborum magnam optio in doloribus ratione esse.</p>
    </div>

    <form class="col-6" action="<?= BASEURL ?>User/tambah" method="post">
        <div class="mb-3">
            <label for="nomor" class="form-label">Nomor Absen</label>
            <input type="text" class="form-control" id="nomor" name="absen" placeholder="Masukkan nomor absen user">
        </div>
        <div class="mb-3">
            <label for="text" class="form-label">Nama User</label>
            <input type="text" class="form-control" id="text" name="nama" placeholder="Masukkan nama user">
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Email address</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan email user">
        </div>
        <div class="mb-3">
            <label for="firstname" class="form-label">Firstname</label>
            <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Masukkan firstname user">
        </div>
        <div class="mb-3">
            <label for="lastname" class="form-label">Lastname</label>
            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Masukkan lastname user">
        </div>
        <button type="submit" class="btn btn-dark">Tambah User</button>
    </form>
</main>